//
//  XPAppDelegate.h
//  XPRemotePubLib
//
//  Created by 向平 on 08/08/2017.
//  Copyright (c) 2017 向平. All rights reserved.
//

@import UIKit;

@interface XPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
