//
//  main.m
//  XPRemotePubLib
//
//  Created by 向平 on 08/08/2017.
//  Copyright (c) 2017 向平. All rights reserved.
//

@import UIKit;
#import "XPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XPAppDelegate class]));
    }
}
