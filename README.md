# XPRemotePubLib

[![CI Status](http://img.shields.io/travis/向平/XPRemotePubLib.svg?style=flat)](https://travis-ci.org/向平/XPRemotePubLib)
[![Version](https://img.shields.io/cocoapods/v/XPRemotePubLib.svg?style=flat)](http://cocoapods.org/pods/XPRemotePubLib)
[![License](https://img.shields.io/cocoapods/l/XPRemotePubLib.svg?style=flat)](http://cocoapods.org/pods/XPRemotePubLib)
[![Platform](https://img.shields.io/cocoapods/p/XPRemotePubLib.svg?style=flat)](http://cocoapods.org/pods/XPRemotePubLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XPRemotePubLib is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "XPRemotePubLib"
```

## Author

向平, 1664272147@qq.com

## License

XPRemotePubLib is available under the MIT license. See the LICENSE file for more info.
